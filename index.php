<!DOCTYPE html>
<?php
require('php/create_db.php');
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>DoppelMau</title>
        
        <script src="js/jquery.min.js"></script>    <!-- Version 3.3.1>-->
        <script src="js/jquery.textfill.min.js"></script>
        <script src="js/functions.js"></script>
        
        <link rel="stylesheet" href="css/mustard-ui.min.css">
        <link rel="stylesheet" href="css/index.css">
    </head>
    <body>
        <div id="content_wrap" class="row">
            <div id="rules_trigger_wrap">
                <img id="rules_trigger" src="img/info-trigger.png" onclick="toggle_rules()">
            </div>
            <div id="rules_wrap">
                <h1>DoppelMau Spielregeln</h1>
                <div id="rules_scroll">
                    <ul id="rules_list">
                        <li>7 = zwei Karten ziehen</li>
                        <li>8 = eine Karte ziehen</li>
                        <li>Bube = Wünscher</li>
                        <li>Ass = Aussetzer</li>
                        <li>Karten (außer Buben) können nur bei gleicher Form oder Farbe aufeinandergelegt werden.</li>
                        <li>Ein Bube kann jederzeit gelegt werden, außer man muss ziehen.</li>
                        <li>Zieher können bei gleicher Farbe oder Form aufeinandergelegt werden, um den <i>nächsten</i> Spieler ziehen zu lassen.</li>
                        <li>Hat man keine passende Karte, muss man eine neue ziehen.</li>
                        <li>Ein Spiel mit zwei Spielern kann nicht durch einen Aussetzer beendet werden.</li>
                        <li>Bei einer verbleibenden Karte, muss MAU <i>einmal</i> gedrückt werden.</li>
                        <li>Nach dem Legen der letzten Karte, muss MAU <i>zweimal</i> gedrückt werden.</li>
                        <li>Wer vergisst, MAU oder MAU-MAU zu sagen, muss eine Strafkarte ziehen, ebenso wenn an einer unpassenden Stelle MAU gesagt wird.</li>
                    </ul>
                </div>
            </div>
            <div id="left" class="col col-sm-2 row">
                <div id="left_top">
                    <div id="btn_start_reset_wrap">
                        <button id="btn_start_reset" class="button-primary button-large" onclick="start_reset_game()">Start</button>
                    </div>
                    <div id="num_start_cards_wrap">
                        <span>Anzahl Startkarten <input id="num_start_cards" type="number" name="num_start_cards" min="2" max="13" value="10"></span>
                    </div>
                </div>
                <div id="left_middle">
                    <div id="num_players_wrap">
                        <span>Spieler: <span id="num_players">0</span>/4</span>
                    </div>
                    <div id="player_list_wrap">
                        <table id="player_list">
                        </table>
                    </div>
                </div>
                <div id="left_bottom">
                    <div id="btn_wrap">
                        <div id="btn_end_turn_wrap">
                            <button id="btn_end_turn" class="button-primary button-large" onclick="end_turn()">Zug beenden</button>
                        </div>
                        <div id="btn_draw_wrap">
                            <button id="btn_draw" class="button-primary button-large" onclick="draw_card()">Ziehen</button>
                        </div>
                        <div id="btn_mau_wrap">
                            <button id="btn_mau" class="button-primary button-large" onclick="mau()">Mau</button>
                        </div>
                    </div>
                    <div id="doppelmau_version_wrap">
                        <span id="doppelmau_version">DoppelMau Closed Alpha</span>
                    </div>
                </div>
            </div>
            <div id="right" class="col col-sm-10 row">
                <div id="right_content_wrap">
                    <div id="right_top">
                        <div id="top_card_wrap">
                        </div>
                    </div>
                    <div id="right_middle">
                        <div id="wished_form_wrap">
                            <span id="wished_form">
                                gewünscht: <span id="wished_html_entity"></span>
                            </span>
                        </div>
                        <div id="hand_cards_wrap">
                        </div>
                    </div>
                    <div id="right_bottom">
                        <span id="info_bar">Willkommen bei DoppelMau!</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="requested_shape_menu_wrap">
            <div id="requested_shape_menu">
                <h1>Ich wünsche mir...</h1>
                <div class="shape_wrap">
                    <img class="shape" id="ka" onclick="set_requested_shape('Ka')" src="img/KaA.png">
                    <img class="shape" id="he" onclick="set_requested_shape('He')" src="img/HeA.png">
                    <img class="shape" id="pi" onclick="set_requested_shape('Pi')" src="img/PiA.png">
                    <img class="shape" id="kr" onclick="set_requested_shape('Kr')" src="img/KrA.png">
                </div>
            </div>
        </div>
    </body>
</html>