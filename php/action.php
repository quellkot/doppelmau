<?php
    session_start();
    require('db.php');
    require('dbobject.php');
    require("spiel.php");
    require("player.php");
    require("card.php");
    $response = array();

    $func = filter_input(INPUT_POST, "func");

    $ini = parse_ini_file("config.ini", true);
    $host = $ini["database"]["host"];
    $user = $ini["database"]["username"];
    $password = $ini["database"]["password"];
    $database = $ini["database"]["database"];

    $con = new mysqli($host, $user, $password, $database);
    if ($con->connect_error)
        die("Verbindung fehlgeschlagen: " . $con->connect_error);
    else
    {
        //Spiel, Spieler und oberste Karte laden
        Spiel::load_data();

        switch($func)
        {
            case "insert_player":
                //Spieler in Datenbank einfügen, sodass er am nächsten Spiel teilnehmen kann
                $player_name = filter_input(INPUT_POST, "player_name");
                
                $response['success'] = true;

                //wenn Spiel bereits gestartet, entsprechende Meldung zurückgeben und abbrechen
                $result = $con->query("SELECT * FROM game");
                if ($result->num_rows > 0)
                {
                    $response['success'] = false;
                    $response['message'] = 'started';
                    break;
                }

                //wenn Name schon vergeben, entsprechende Meldung zurückgeben und abbrechen
                $result = $con->query("SELECT * FROM players WHERE name='$player_name'");
                if ($result->num_rows > 0)
                {
                    $response['success'] = false;
                    $response['message'] = 'double_name';
                    break;
                }

                //Session ID (eindeutige Identifikation)
                $session_id = session_id();

                //wenn noch kein Spieler in DB, wird erster Spieler zum Host mit Startnummer 1
                //sind schon vier Spieler da, Meldung "full" zurückgeben und abbrechen
                //ansonsten hinzufügen, nicht als Host und mit nächsthöherer Startnummer
                $result = $con->query("SELECT * FROM players");
                if ($result->num_rows == 0)
                    $con->query("INSERT INTO players (session_id, name, start_num, host) VALUES ('$session_id', '$player_name', 1, 1)");
                elseif($result->num_rows >= 4)
                {
                    $response['success'] = false;
                    $response['message'] = 'full';
                }
                else
                    $con->query("INSERT INTO players (session_id, name, start_num, host) SELECT '$session_id', '$player_name', MAX(start_num)+1, 0 FROM players");
                
                break;
            case "start_game":
                $num_start_cards = filter_input(INPUT_POST, 'num_start_cards');
                
                $response["success"] = true;

                //verhindern, dass das Spiel gestartet wird, wenn nur 1 Spieler da ist
                $result = $con->query("SELECT * FROM players");
                if ($result->num_rows < 2)
                {
                    $response["success"] = false;
                    $response["message"] = "too_few_players";
                    break;
                }

                //wenn noch kein Spiel existiert, eines erstellen
                //sonst zurückgeben, dass bereits eins erstellt wurde
                $result = $con->query("SELECT * FROM game");
                if ($result->num_rows == 0)
                {
                    $con->query("INSERT INTO game (status, draw_counter, mau_counter, turn_stage, turn, info) VALUES (0, 0, 0, 0, 0, 'Spiel wird initialisiert, Karten werden gemischt ...')");
                    
                    //zufällig nächsten Spieler auswählen
                    Spiel::$config['turn'] = 0;
                    Spiel::next_player(true);
                }
                else
                {
                    $response["success"] = false;
                    $response["message"] = "game_started";
                    break;
                }

                //jedem Spieler Startkarten zuweisen (= Karten geben)
                //Anzahl abhängig von durch Host bestimmte Startkartenanzahl
                $result = $con->query("SELECT * FROM players");

                var_dump($result);

                for($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    $id = $row["id"];
                    $con->query("UPDATE cards SET status=1, on_hand=$id WHERE on_hand IS NULL ORDER BY RAND() LIMIT $num_start_cards");
                }

                //bei einer weiteren zufälligen Karte den Status auf 2 setzen (= eine Karte aufdecken)
                $con->query("UPDATE cards SET status=2 WHERE status=0 ORDER BY RAND() LIMIT 1");

                break;
            case "reset":
                //Status und "Auf-wessen-Hand"-Wert der Karten auf Initialwert zurüksetzen
                //Inhalt der Tabellen players und game löschen
                $con->query("UPDATE cards SET status = 0, on_hand=null");
                $con->query("DELETE FROM players");
                $con->query("ALTER TABLE players AUTO_INCREMENT = 1");
                $con->query("TRUNCATE game");
                break;
            case "kickplayer":
                //löscht Spieler aus der players-Tabelle und setzt gekickt-Info
                $name = filter_input(INPUT_POST, "name");
                $sql = $con->query("SELECT * FROM players WHERE name='$name'");
                $player = $sql->fetch_assoc();
                
                //falls gekickter Spieler gerade dran ist
                if($player['start_num'] == Spiel::$config['turn'])
                    Spiel::next_player();

                $con->query("DELETE FROM players WHERE name='$name'");
                Spiel::set_info("$name wurde gekickt");

                break;
            case "end_turn":
                $response = Player::end_turn();
                break;
            case "draw_card":
                $response = Player::draw_cards();
                break;
            case "play_card":
                $card = filter_input(INPUT_POST, "card");
                $response = Player::play_card($card);
                break;
            case "mau":
                $result = $con->query("SELECT * FROM game");
                $row = $result->fetch_assoc();
                if(intval($row["mau_counter"]) < 2)
                    $con->query("UPDATE game SET mau_counter = mau_counter + 1");
                break;
            case "set_requested_shape":
                $shape = filter_input(INPUT_POST, "shape");
                $con->query("UPDATE game SET requested_shape='$shape', turn_stage=3");
                break;
        }
    }
    
    //Ausgabe des Antwort-Arrays (ist dadurch der Rückgabewert, der aufrufenden Ajax-Funktion
    header('Content-Type: application/json');
    echo json_encode($response);
?>