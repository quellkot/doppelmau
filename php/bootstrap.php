<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src"), $isDevMode);
// database configuration parameters
$connectionParams = array(
    'dbname' => 'doppelmau',
    'user' => 'doppelmau',
    'password' => 'doppelmau',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
);
//obtaining the entity manager
$entityManager = EntityManager::create($connectionParams, $config);
