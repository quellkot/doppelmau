<?php
/**
 * @Entity @Table(name="cards")
 **/
class Card
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") */
    protected $form;
    /** @Column(type="string") */
    protected $value;

    protected $status;

    protected $on_hand;

    public function getId()
    {
        return $this->id;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getOnHand()
    {
        return $this->on_hand;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param mixed $on_hand
     */
    public function setOnHand($on_hand)
    {
        $this->on_hand = $on_hand;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }
}