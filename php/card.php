<?php

class Card
{
    const IN_DECK = 0;
    const IN_HAND = 1;
    const ON_TOP = 2;
    const IN_GRAVEYARD = 3;
}