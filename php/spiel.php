<?php

class Spiel
{
    const TS_BEFORE = 0;
    const TS_PLAYED = 1;
    const TS_HAS_TO_WISH = 2;
    const TS_DRAWN = 3;

    public static $this_player;
    public static $card_top;
    public static $config;
    
    /**
     * Lädt die Daten des Spiels,
     * des Spielers, welcher die Funktion aufruft und
     * der obersten Karte
     */
    public static function load_data($player_name = NULL){
        $db = DB::get_db();

        //Spieldaten
        self::$config = $db->select_first('game');

        //aktueller Spieler
        if($player_name === NULL){
            $session = session_id();
            self::$this_player = $db->select_first('players', "session_id = '$session'");
        } 
        else {
            self::$this_player = $db->select_first('players', "name = '$player_name'");
        }
        
        //oberste Karte
        self::$card_top = $db->select_first('cards', "status = '" . Card::ON_TOP . "'");
    }


    public static function start_game($num_start_cards)
    {
        $db = DB::get_connection();
        $sql = $db->query("SELECT id, turn FROM players");
        $players = $sql->fetchAll(PDO::FETCH_ASSOC, 0);
        $response = array(
            'success' => false
        );

        //verhindern, dass das Spiel gestartet wird, wenn nur 1 Spieler da ist
        if (sizeof($players) < 2)
        {
            $response["message"] = "too_few_players";
            return $response;
        }

        //jedem Spieler Startkarten zuweisen (= Karten geben)
        //Anzahl abhängig von durch Host bestimmte Startkartenanzahl
        for($i = 0; $i < sizeof($players); $i++)
        {
            Player::draw_cards($players[$i],  $num_start_cards);
        }

        //wenn noch kein Spiel existiert, eines erstellen
        //sonst zurückgeben, dass bereits eins erstellt wurde <-- evtl stattdessen Fehler zurückgeben und Startknopf ausblenden
        if (!Spiel::create_game_db())
        {
            $response["message"] = "game_started";
            return $response;
        }

        //bei einer weiteren zufälligen Karte den Status auf 2 setzen (= eine Karte aufdecken)
        $db->query("UPDATE cards SET status=2 WHERE status=0 ORDER BY RAND() LIMIT 1");
        
        $response["success"] = true;
        return $response;
    }

    /**
     * @return bool ob das Spiel erfolgreich erstellt werden konnte
     */
    private static function create_game_db()
    {
        //zufällige Startnummer zwischen 1 und 4 bestimmen
        //Idee: wenn nicht so viele Spieler da sind, wird übersprungen, z.B. drei Spieler sind da, der Vierte wäre dran --> Spieler 1 ist dran
        $start = rand(1, 4);

        $db = DB::get_connection();
        $result = $db->query("SELECT COUNT(*) FROM game");
        if (($result->fetch(PDO::FETCH_NUM))[0] != 0){
            return false;
        }
        $db->query("INSERT INTO game (status, requested_form, draw_counter, turn, info) VALUES (0, NULL, 0, $start, 'Spiel wird initialisiert, Karten werden gemischt ...')");
        return true;
    }
    
    
     /**
      * Prüft ob die zu legende Karte mit der obersten Karte kompitabel ist 
      * (Buben sind immer erlaubt)
      * @param Card $card
      * @return bool
      */
     public static function check_card($card){
        //gewünschte Farbe ermitteln
        if(self::$config['requested_shape']){
            self::$card_top['form'] = self::$config['requested_shape'];
        }
        return self::$card_top['form'] === $card['shape'] || self::$card_top['value'] === $card['value'] || $card['value'] === "B";
     }

     public static function shuffle_cards(){
        $db = DB::get_connection();
        $sql = $db->prepare("UPDATE cards SET status = 0 WHERE status = '3'");
        $success = $sql->execute();
        if(!$success){
            return false;
        }
        return true;   
     }

     /**
      * @return int
      */
     public static function anzahl_spieler(){
        $db = DB::get_connection();
        $sql = $db->prepare("SELECT COUNT(*) FROM players");
        $success = $sql->execute();
        if(!$success){
            return 0;
        }
        $anzahl = ($sql->fetch(PDO::FETCH_NUM))[0];
        return $anzahl;   
     }

     public static function next_player($random = false){
        $db = DB::get_connection();

        $order = $random ? "RAND()" : "start_num";
        
        //start_num von nächstem Spieler herausfinden
        $get = $db->prepare("SELECT * FROM players ORDER BY (start_num <= ?), " . $order);
        $get->execute(array(Spiel::$config["turn"]));
        $new_player = $get->fetch(PDO::FETCH_ASSOC);

        //bei gerade gelegtem Ass übernächster Spieler
        if(Spiel::$card_top['value'] == 'A' && Spiel::$config['turn_stage'] == Spiel::TS_PLAYED){
            $msg = $new_player['name'] . ' muss aussetzen. ';
            $new_player = $get->fetch(PDO::FETCH_ASSOC);
            Spiel::set_info($msg . $new_player['name'] . ' ist an der Reihe.');
        }
//        else
//            Spiel::set_info($new_player['name'] . ' ist an der Reihe.');

        //turn auf start_num setzen
        $set = $db->prepare("UPDATE game SET turn = ?, turn_stage = '0'");
        $set->execute(array($new_player["start_num"]));

        //mau_counter zurücksetzen
        $db->query("UPDATE game SET mau_counter = 0");
        
        return $new_player;
     }

     public static function set_info($infotext){
        $db = DB::get_connection();
        $sql = $db->prepare('UPDATE game SET info = ?');
        $sql->execute(array($infotext));
     }
}