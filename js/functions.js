var old_self_cards = [];
var old_open_card = [];
var turn_stage_glob = 0;
var my_turn_glob = false;
var game_started_glob = false;
var am_i_host_glob = false;

function clean(node)
{
    for (var n = 0; n < node.childNodes.length; n++)
    {
        var child = node.childNodes[n];
        if (child.nodeType === 8 || (child.nodeType === 3 && !/\S/.test(child.nodeValue)))
        {
            node.removeChild(child);
            n--;
        }
        else if (child.nodeType === 1)
            clean(child);
    }
}

function toggle_rules()
{
    //macht Regelanzeige sichtbar, wenn sie gerade nicht angezeigt wird und umgekehrt  
    if($("#rules_wrap").css("display") === "none")
        $("#rules_wrap").css("display", "flex");
    else
        $("#rules_wrap").css("display", "none");
}

function start_reset_game()
{
    //startet das Spiel, falls es noch nicht gestartet wurde
    //setzt das Spiel zurück (bzw. löscht es), falls es gestartet wurde
    if(game_started_glob === false)
    {
        var num_start_cards = $("#num_start_cards").val();
        $.ajax({
            url: "php/action.php",
            type: "POST",
            data: {
                func: "start_game",
                num_start_cards: num_start_cards
            },
            success: function(result) {            
                if(result.success === false)
                {
                    if(result.message === "too_few_players")
                        alert("Bitte warten, bis weitere Spieler beigetreten sind.");

                    if(result.message === "game_started")
                        alert("Das Spiel wurde bereits gestartet");
                }
            }
        });
    }
    else
    {
        $.ajax({
            url: "php/action.php",
            type: "POST",
            data: {
                func: "reset"
            }
        });
    }
}

function end_turn()
{
    //beendet Zug des Spielers
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "end_turn"
        }
    });
}

function draw_card()
{
    //lässt den Spieler eine Karte ziehen, falls er keine passende hat
    //oder so viele wie eer laut draw_counter ziehen muss
    var player_name = get_player_name_from_cookie();
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "draw_card",
            player_name: player_name
        }
    });
}

function mau()
{
    //Spieler klickt auf den Mau-Knopf
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "mau"
        }
    });
}

function set_requested_shape(shape)
{
    //die gewünschte Form wird in der Datenbank gesetzt
    turn_stage_glob = 3; //damit das Wünsch-Menü nicht nochmal kurz erscheint
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "set_requested_shape",
            shape: shape
        }
    });
    
    $("#requested_shape_menu_wrap").css("display", "none");
}

function refresh_info(info)
{
    //fügt Info in dafür vorgsehenes DIV
    $("#info_bar").html(info);
}

function get_player_name_from_cookie()
{
    //den im Cookie gespeicherten Spielernamen zurückgeben
    var regex = new RegExp("player_name=([^;]+)");
    var value = regex.exec(document.cookie);
    return (value !== null) ? unescape(value[1]) : "";
}

function get_player_name(msg)
{
    //wenn es nicht schon einen Cookie mit dem Spielernamen gibt, wird einer erstellt und der Spieler nach einem Name gefragt
    if(!(document.cookie).includes("player_name"))
    {
        var player_name = prompt(msg);
        if(player_name === null) //wenn auf "Abbrechen" geklickt, wiederholen
            get_player_name("Ein Spielername ist erforderlich.");
        else if(player_name === "") //wenn leerer String als Name eingegeben, wiederholen
            get_player_name("Spielername kann nicht leer sein.");
        else if(/[^a-zA-Z0-9]/.test(player_name)) //wenn Name Sonder- oder Leerzeichen enthält, wiederholen
            get_player_name("Bitte keine Sonder- oder Leerzeichen verwenden.");
        else
        {
            //Cookie erstellen
            document.cookie = "player_name=" + player_name;
            //Spielername wird an PHP übergeben, um in die DB eingefügt zu werden
            $.ajax({
                url: "php/action.php",
                type: "POST",
                async: false, //um z.B. in Firefox den Error "NS_ERROR_XPC_SECURITY_MANAGER_VETO:" zu vermeiden
                data: {
                    func: "insert_player",
                    player_name: player_name
                },
                success: function(result) {
                    if(result.success)
                        return;

                    switch(result.message)
                    {
                        case "double_name":
                            //gibt es bereits einen Spieler mit diesem Namen, wird der Cookie gelöscht und der Spieler erneut nach einem Namen gefragt
                            document.cookie = "player_name=; Max-Age=-99999999;";
                            get_player_name("Dieser Name wird bereits verwendet, bitte wähle einen anderen.");
                            break;
                        case "full":
                            //sind bereits vier Spieler im Spiel, wird der Cookie gelöscht und der Spieler erneut nach einem Namen gefragt
                            document.cookie = "player_name=; Max-Age=-99999999;";
                            get_player_name("Kein Platz mehr. Versuche es später nochmal.");
                            break;
                        case "started":
                            //ist das Spiel bereits gestartet, wird der Cookie gelöscht und der Spieler erneut nach einem Namen gefragt
                            document.cookie = "player_name=; Max-Age=-99999999;";
                            get_player_name("Das Spiel wurde bereits gestartet. Warte bis es vorbei ist und versuche es nochmal.");
                            break;
                    }
                }
            });
        }
    }
    
    return player_name;
}

function add_player_to_sidebar(players, turn, pcount)
{
    //Inhalt der Spielerliste löschen
    $("#player_list_wrap table").html("");
    
    //HTML-Element mit Anzahl der Spieler füllen
    $("#num_players").html(pcount);
    
    //für jeden Spieler im Array eine Zeile erstellen in die Spielerliste einfügen
    var p, i = 0;
    for(i; i < players.length; i++)
    {
        p = players[i];
        
        var host_class = "";
        var turn_class = "";
        var kickplayer_div = "";
        if(p.host === "1")
            host_class = " host";
        if(turn === p.start_num)
            turn_class = " turn";
        if(am_i_host_glob === true && p.host !== "1")
            kickplayer_div = "<div class='kickplayer' onclick=\"kickplayer('" + p.name + "')\">X</div>";

        var player_html_append = "<tr><td class='player" + turn_class + host_class + "'><span class='player_name'>" + p.name + "</span><span class='player_card_num'> (" + p.num_cards + ")</span></td><td>" + kickplayer_div + "</td></tr>";
        $("#player_list_wrap table").append(player_html_append);
    }
}

function display_cards(self_cards, open_card)
{
    if(typeof self_cards === "undefined")
        return;
    
    //vergleicht den Inhalt der Karten in der Hand mit dem Stand beim letzten Aufruf
    //bei Gleichheit wird die Anzeige nicht erneuert (würde zu Flackern führen)
    if(old_self_cards.length === self_cards.length && old_self_cards.sort().every(function(value, index) { return value === self_cards.sort()[index];}) && old_open_card === open_card)
        return;
    
    old_self_cards = self_cards;
    old_open_card = open_card;
    var append_string = "";
    $.each(self_cards, function(i, val){
        append_string += "<input type='radio' class='hand_cards_chbx' name='hand_cards_chbx' id='hand_cards_chbx"+i+"' value='"+i+"'>";
        append_string += "<label for='hand_cards_chbx"+i+"'>";
        append_string += "<img class='hand_card' src='img/"+val+".png' alt='"+val+"' onclick='play_card(\""+val+"\")'>";
        append_string += "</label>";
    });
    $("#hand_cards_wrap").html(append_string);
    
    var open_card_img = "<img id='top_card' src='img/"+open_card+".png' alt='"+open_card+"'>";
    $("#top_card_wrap").html(open_card_img);
}

function display_requested_shape(requested_shape)
{
    //zeigt die gewüschte Form im dafür vorgesehenen DIV
    var shape;
    if(requested_shape !== null && typeof requested_shape !== "undefined")
    {
        switch(requested_shape)
        {
            case "Pi": shape = "<span style='color:black'>&spades;</span>"; break;
            case "Kr": shape = "<span style='color:black'>&clubs;</span>"; break;
            case "He": shape = "<span style='color:red'>&hearts;</span>"; break;
            case "Ka": shape = "<span style='color:red'>&diams;</span>"; break;
        }
        $("#wished_form").css("display", "block");
        $("#wished_html_entity").html(shape);
    }
    else
        $("#wished_form").css("display", "none");
}

function requested_shape_menu()
{
    //zeigt das Menü zur Auswahl einer Form beim Legen eines Buben
    if(my_turn_glob === true && turn_stage_glob === 2)
        $("#requested_shape_menu_wrap").css("display", "flex");
    else
        $("#requested_shape_menu_wrap").css("display", "none");
}

function display_game_controls()
{
    //ändert Anzeige im Start/Reset-Knopf, je nachdem ob Spiel gestartet oder nicht
    if(game_started_glob === true)
        $("#btn_start_reset").html("Reset");
    else
        $("#btn_start_reset").html("Start");
    
    //zeigt die Buttons zum Interagieren, wenn man am Zug ist
    if(my_turn_glob === true)
    {
        console.log("my_turn: "+ my_turn_glob +" - turn_stage: "+ turn_stage_glob);
        
        $("#btn_mau").css("display", "block");
        if(turn_stage_glob === 0)
        {
            $("#btn_draw").css("display", "block");
            $("#btn_end_turn").css("display", "none");
        }
        else
        {
            $("#btn_draw").css("display", "none");
            $("#btn_end_turn").css("display", "block");
        }
    }
    else
    {
        $("#btn_end_turn").css("display", "none");
        $("#btn_draw").css("display", "none");
        $("#btn_mau").css("display", "none");
    }
}

function display_host_controls()
{
    //zeigt den Start/Reset Button, wenn man Host ist
    //zeigt außerdem das Input Element zur Auswahl der Startkartenanzahl, wenn das Spiel noch nicht gestartet wurde
    if(am_i_host_glob === true)
    {
        $("#btn_start_reset").css("display", "block");
        if(game_started_glob === false)
            $("#num_start_cards_wrap span").css("display", "block");
        else
            $("#num_start_cards_wrap span").css("display", "none");
    }
    else
    {
        $("#btn_start_reset").css("display", "none");
        $("#num_start_cards_wrap span").css("display", "none");
    }
}

function kickplayer(name)
{
    //Name des zu kickenden Spielers an PHP übergeben
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "kickplayer",
            name: name
        }
    });
}

function play_card(card)
{
    //noch nicht beim ersten Klick, damit erst beim zweiten Klick die Funktion ausgeführt wird
    var checked_card = $(".hand_cards_chbx:checked + label > img").attr("alt");
    if(checked_card !== card)
        return;

    //zu spielende Karte an Server senden
    $.ajax({
        url: "php/action.php",
        type: "POST",
        data: {
            func: "play_card",
            card: card
        }
    });
}

function adjust_font_sizes()
{
    //Anpassen der Schriftgröße einiger Elemente an den zur Verfügung stehenden Platz
    //Text passt sich damit dynamisch verschiedenen Bildschirmgrößen an
    $('#right_bottom').textfill({ maxFontPixels: -1 });
    $('#timer_wrap').textfill({ maxFontPixels: -1 });
    $('#doppelmau_version_wrap').textfill({ maxFontPixels: '18px' });
    $('#wished_form_wrap').textfill({ maxFontPixels: -1 });
    $('#num_start_cards_wrap').textfill({ maxFontPixels: -1 });
    $('#num_players_wrap').textfill({ maxFontPixels: -1 });
    $('#player_list_wrap table tr').textfill({ maxFontPixels: -1, innerTag: 'td' });
}

function eternal_bgcheck(index)
{
    //Spielername im Cookie auslesen
    var player_name = get_player_name_from_cookie();
    $.ajax({
        url: "php/bg.php",
        type: "POST",
        data: {
            func: "check",
            player_name: player_name
        },
        success: function(result) {
//            console.log(result.players);
            
            //Cookie löschen, wenn Spielername nicht mehr in DB
            if(!result.player_in_db)
            {
                document.cookie = "player_name=; Max-Age=-99999999;";
                location.reload();
            }
            
            //Infoleiste befüllen
            if(result.hasOwnProperty('info'))
                refresh_info(result.info);
            
            //Spielerliste mit den Namen der Spieler befüllen
            add_player_to_sidebar(result.players, result.turn, result.pcount);
            
            //Buttons zum Zug beenden, Ziehen und Mau anzeigen, wenn Spiel gestartet
            display_game_controls();
            
            //die Steuerelemente für den Host sichtbar machen
            display_host_controls();
            
            //eigene und aufgedeckte Karten anzeigen
            display_cards(result.self_cards, result.open_card);
            
            //gewünschte Form anzeigen
            display_requested_shape(result.requested_shape);

            //Wünscher Auswahlmenü anzeigen
            requested_shape_menu();
            
            //Anzahl zu ziehender Karten in Ziehen-Button schreiben (wenn mehr als eine)
            if(Number(result.draw_counter) > 0)
                $("#btn_draw").html("Ziehen (" + result.draw_counter + ")");
            else
                $("#btn_draw").html("Ziehen");

            //Anzahl an gesagten Mau's anzeigen
            if(Number(result.mau_counter) > 0)
                $("#btn_mau").html("Mau (" + result.mau_counter + ")");
            else
                $("#btn_mau").html("Mau");
            
            //Schriftgrößen anpassen
            adjust_font_sizes();
            
            am_i_host_glob = result.host === true;
            game_started_glob = result.game_started === true;
            my_turn_glob = result.my_turn === true;
            turn_stage_glob = parseInt(result.turn_stage);

            //nach einer Sekunde den Hintergrundcheck erneut ausführen
            setTimeout(function(){
                eternal_bgcheck(++index);
            }, 500);
        }
    });
}

//verstecken des Spielregel-DIVs, wenn woanders geklickt
$(document).click(function(event) {
    if (!$(event.target).is("#rules_trigger, #rules_wrap *")) {
        $("#rules_wrap").css("display", "none");
    }
});

//wenn die Seite vollständig geladen wurde...
$(document).ready(function() {
    //HTML der gesamten Seite von unsichtbaren aber den Bildlauf zerstörenden #text-Elementen säubern
    clean(document.body);
    //den Nutzer einen Spielernamen wählen lassen, falls nicht schon getan
    get_player_name("Willkommen bei DoppelMau! Gib einen Spielernamen ein:");
    
    //sekündlicher Check nach veränderten Spieldaten, neuen Spielern, neuen Infos usw.
    eternal_bgcheck(1);
});